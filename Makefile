DESTDIR=/

all: install

install:
	mkdir -p $(DESTDIR)/etc/xdg/ || true
	mkdir -p $(DESTDIR)/etc/skel/ || true
	mkdir -p $(DESTDIR)/usr/share/glib-2.0/schemas/ || true
	
	cp -prfv xdg/* $(DESTDIR)/etc/xdg/
	cp -prfv skel/* $(DESTDIR)/etc/skel/
	cp -prfv schemas/* $(DESTDIR)/usr/share/glib-2.0/schemas/
